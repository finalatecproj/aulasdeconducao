@extends('layouts.app')

@section('title')
<title>{{ $vehicle->id }} - {{ ucfirst($vehicle->model) }}</title>
@endsection

@section('css')
@endsection

@section('content')
<div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>
                        <b>Modelo</b>
                        {{ ucfirst($vehicle->model) }}
                    </p>
                    <p>
                        <b>Ano</b>
                        {{ $vehicle->year }}
                    </p>
                    <p>
                        <b>Km</b>
                        {{ $vehicle->km }}
                    </p>
                    <p>
                        <b>Marca</b>
                        {{ $vehicle->brand }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection