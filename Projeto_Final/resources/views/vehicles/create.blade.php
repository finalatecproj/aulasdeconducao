@extends('layouts.app')

@section('title')
<title>Criar Veiculo</title>
@endsection

@section('css')
@endsection

@section('content')
<div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/vehicles">
                        @include('vehicles.form')
                        
                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label for="category_id" class="col-md-4 control-label">Categoria</label>
                            <div class="col-md-6">
                                <select class="form-control" name="category_id" required>
                                    <option value="" disabled selected>Escolha uma categoria</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                @if(Session::has('category_id'))
                                    <span class="help-block">
                                        <strong>{{ Session::get('category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <input class="btn btn-xs btn-primary" type="submit" value="Submeter">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
@endsection
