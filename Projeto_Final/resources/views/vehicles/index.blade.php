@extends('layouts.app')

@section('title')
<title>Vehicles</title>
@endsection

@section('css')
@endsection

@section('content')
    <p>
        <a href="/vehicles/create" class="btn btn-success">Criar Veiculo</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">Lista de todos os Veiculos</div>

        <div class="panel-body table-responsive">
        {{--  remove datatable if dont want the entries and other stuff  --}}
            <table id="datatable" class="table table-bordered table-striped {{ count($vehicles) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Modelo</th>
                        <th>Ano</th>
                        <th>Km</th>
                        <th>Categoria</th>
                        <th>Marca</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($vehicles) > 0)
                        @foreach ($vehicles as $vehicle)
                            <tr data-entry-id="{{ $vehicle->id }}">
                                <td>{{ ucfirst($vehicle->model) }}</td>
                                <td>{{ $vehicle->year }}</td>
                                <td>{{ $vehicle->km }}</td>
                                <td>{{ $vehicle->category->name }}</td>
                                <td>{{ ucfirst($vehicle->brand) }}</td>
                                <td>
                                    <a href="/vehicles/{{ $vehicle->id }}" class="btn btn-xs btn-primary">Ver</a>
                                    <a href="/vehicles/{{ $vehicle->id }}/edit" class="btn btn-xs btn-info">Editar</a>
                                    <form method="post" action="/vehicles/{{ $vehicle->id}}" style="display: inline-block;">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <input type="submit" class="btn btn-xs btn-danger" value="Apagar">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">Não existem Veiculos</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection