@extends('layouts.app')

@section('content')
<div>
    <div class="row" style="text-align:center;">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Bem vindo {{ucfirst(Auth::User()->name)}}!</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    A partir de agora encontra-se logado na nossa aplicação.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
