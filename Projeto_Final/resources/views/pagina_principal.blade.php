<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"> <!--<![endif]-->
	<html lang="{{ app()->getLocale() }}">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Escola de Condução Guifões</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />


	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,600,400italic,700' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="myfirstpage/layout/css/animate.css">
	<!-- Flexslider -->
	<link rel="stylesheet" href="myfirstpage/layout/css/flexslider.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="myfirstpage/layout/css/icomoon.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="myfirstpage/layout/css/magnific-popup.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="myfirstpage/layout/css/bootstrap.css">

	<!--Default Theme Style You can change the style.css (default color purple) to one of these styles-->
	<link rel="stylesheet" href="myfirstpage/layout/css/style.css">



	


	<!-- Modernizr JS -->
	<script src="myfirstpage/layout/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>

	<body>
	
	<!-- Loader -->
	<!--uncomment this for load-->
	<!--div class="fh5co-loader"></div-->
	
	<div id="fh5co-page" style="background: #efefef url(myfirstpage/layout/images/white_wave.png) repeat;">
		<section id="fh5co-header">
			<div class="container">
				<nav role="navigation">
				@if(auth::user() && auth::user()->is_admin == 1)
					<h1 id="fh5co-logo"><a href="/">Escola de Condução Guifões<span>.</span></a></h1>
					<ul class="pull-right right-menu">
						<li class="fh5co-cta-btn dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								{{ ucfirst (Auth::user()->name) }} <span class="caret"></span>
							</a>

							<ul class="dropdown-menu" role="menu">
								<li class="fh5co-cta-btn">
									<a href="{{ route('logout') }}"
										onclick="event.preventDefault();
													document.getElementById('logout-form').submit();">
										Logout
									</a>

									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										{{ csrf_field() }}
									</form>
								</li>
							</ul>
						</li>
                    </ul>
				@else
					<ul class="pull-right right-menu">
						<li><a href="/login">Login</a></li>
					</ul>
					<h1 id="fh5co-logo"><a href="/">Escola de Condução Guifões<span>.</span></a></h1>
				@endif
				</nav>
			</div>
		</section>
		<!-- #fh5co-header -->
		<section id="fh5co-hero" class="js-fullheight" style="background-image: url(images/hero_bg.jpg);" data-next="yes">
			<div class="fh5co-overlay"></div>
			<div class="container">
				<div class="fh5co-intro js-fullheight">
					<div class="fh5co-intro-text">
						<!-- 
							INFO:
							Change the class to 'fh5co-right-position' or 'fh5co-center-position' to change the layout position
							Example:
							<div class="fh5co-right-position">
						-->
						<div class="fh5co-left-position">
							<h2 class="animate-box">A formar os melhores condutores desde 1998</h2>
							 
						</div>
					</div>
				</div>
			</div>
			<div class="fh5co-learn-more animate-box">
				<a href="#" class="scroll-btn">
					<span class="text">Descubra mais sobre nós</span>
					<span class="arrow"><i class="icon-chevron-down"></i></span>
				</a>
			</div>
		</section>
		<!-- END #fh5co-hero -->

		<section id="fh5co-features-2">
			<div class="container" style="padding:20px;">
				
				<div class="col-md-6 col-md-6">
					<h2 class="fh5co-lead animate-box">Vantagens</h2>
					<div class="fh5co-feature">
						<div class="fh5co-icon animate-box"><i class="icon-check2"></i></div>
						<div class="fh5co-text animate-box">
							<h3>Disponibilidade</h3>
						</div>
					</div>
					<div class="fh5co-feature">
						<div class="fh5co-icon animate-box"><i class="icon-check2"></i></div>
						<div class="fh5co-text animate-box">
							<h3>Ambiente amigável</h3>
						</div>
					</div>
					<div class="fh5co-feature">
						<div class="fh5co-icon animate-box"><i class="icon-check2"></i></div>
						<div class="fh5co-text animate-box">
							<h3>Diversidade</h3>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- END #fh5co-features-2 -->
		

		
		<section id="fh5co-pricing">
			<div class="container" style="padding:50px;">
				<div class="row">
					<div class="col-md-3 col-sm-6 animate-box">
					<div class="price-box">
						<h2 class="pricing-plan">Categoria A</h2>
						<div class="price"><sup class="currency">€</sup>199,99</div>
						<hr>
						<ul class="pricing-info">
							<li>Idade Minima: 24 anos</li>
							<li>Lições teóricas: ilimitadas</li>
							<li>Lições Práticas: 12</li>
							<li>Pagamento 2x sem juros</li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 animate-box">
					<div class="price-box">
						<h2 class="pricing-plan">Categoria A1</h2>
						<div class="price"><sup class="currency">€</sup>249,99</div>
						<hr>
						<ul class="pricing-info">
							<li>Idade Minima: 16 anos</li>
							<li>Lições teóricas: ilimitadas</li>
							<li>Lições Práticas: 12</li>
							<li>Pagamento a pronto</li>
						</ul>
					</div>
				</div>
				<div class="clearfix visible-sm-block"></div>
				<div class="col-md-3 col-sm-6 animate-box">
					<div class="price-box popular">
						<div class="popular-text">Promoção</div>
						<h2 class="pricing-plan">Categoria B</h2>
						<div class="price"><sup class="currency">€</sup>550</div>
						<hr>
						<ul class="pricing-info">
							<li>Idade Minima: 18 anos</li>
							<li>Lições teóricas: ilimitadas</li>
							<li>Lições Práticas: 32</li>
							<li>Pagamento 2x sem juros</li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 animate-box">
					<div class="price-box">
						<h2 class="pricing-plan">Categoria B1</h2>
						<div class="price"><sup class="currency">€</sup>274,00</div>
						<hr>
						<ul class="pricing-info">
							<li>Idade Minima: 16 anos</li>
							<li>Lições teóricas: ilimitadas</li>
							<li>Lições Práticas: 12</li>
							<li>Pagamento 2x sem juros</li>
						</ul>
					</div>
				</div>
				
				</div>
			</div>
		</section>
		<!-- END #fh5co-subscribe -->

		<footer id="fh5co-footer">
			<div class="container col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px;padding-right:0px">

					<div class="col-md-12 col-sm-12 col-xs-12 animate-box" style="margin:0px auto;padding:0px;">
						<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
						<div style='overflow:hidden;height:300px;width:100;'><div id='gmap_canvas' 
						style='height:300px;width:100%;'></div><div><small><a href="http://www.googlemapsgenerator.com/es/"
						>http://www.googlemapsgenerator.com/es/</a></small></div><div><small><a href="https://ww.buywebtraf
						ficexperts.com/">http://BuyWebTrafficExperts.com</a></small></div><style>#gmap_canvas 
						img{max-width:none!important;background:none!important}</style></div><script type='text/javascript'>
						function init_map(){var myOptions = {zoom:10,center:new google.maps.LatLng(41.1923767,-8.668572199999971)
						,mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas')
						, myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(41.1923767,-8.668572199999971)});infowindow = 
						new google.maps.InfoWindow({content:'<strong>Encontre-nos em</strong><br>Av. Joaquim Neves dos Santos 697, Guifões<br>'});google.maps.event.addListener
						(marker, 'click', function(){infowindow.open(map,marker);});
						infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
					</div>
			
			</div>
		</footer>
		<!-- END #fh5co-footer -->
	</div>
	<!-- END #fh5co-page -->
	
	
	<!-- jQuery -->
	<script src="myfirstpage/layout/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="myfirstpage/layout/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="myfirstpage/layout/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="myfirstpage/layout/js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="myfirstpage/layout/js/jquery.flexslider-min.js"></script>
	<!-- Magnific Popup -->
	<script src="myfirstpage/layout/js/jquery.magnific-popup.min.js"></script>
	<script src="myfirstpage/layout/js/magnific-popup-options.js"></script> 


	<!-- Main JS (Do not remove) -->
	<script src="myfirstpage/layout/js/main.js"></script>

	

	</body>
</html>

