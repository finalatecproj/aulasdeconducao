@extends('layouts.app')

@section('title')
<title>Instrutores</title>
@endsection

@section('css')
@endsection

@section('content')
    <p>
        <a href="/instructors/create" class="btn btn-success">Criar Instrutor</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">Lista de todos os Instrutores</div>

        <div class="panel-body table-responsive">
        {{--  remove datatable if dont want the entries and other stuff  --}}
            <table id="datatable" class="table table-bordered table-striped {{ count($instructors) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Numero de Telefone</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($instructors) > 0)
                        @foreach ($instructors as $instructor)
                            <tr data-entry-id="{{ $instructor->id }}">
                                <td>{{ ucfirst($instructor->name) }}</td>
                                <td>{{ $instructor->email }}</td>
                                <td>{{ $instructor->phone }}</td>
                                <td>
                                    <a href="/instructors/{{ $instructor->id }}" class="btn btn-xs btn-primary">Ver</a>
                                    <a href="/instructors/{{ $instructor->id }}/edit" class="btn btn-xs btn-info">Editar</a>
                                    <form method="post" action="/instructors/{{ $instructor->id}}" style="display: inline-block;">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <input type="submit" class="btn btn-xs btn-danger" value="Apagar">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">Não existem instrutores</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection