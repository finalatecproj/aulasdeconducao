@extends('layouts.app')

@section('title')
<title>{{ $instructor->id }} - {{ ucfirst($instructor->name) }}</title>
@endsection

@section('css')
@endsection

@section('content')
<div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>
                        <b>Nome</b>
                        {{ ucfirst($instructor->name) }}
                    </p>
                    <p>
                        <b>E-mail</b>
                        {{ $instructor->email }}
                    </p>
                    <p>
                        <b>Numero de telefone</b>
                        {{ $instructor->phone }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection