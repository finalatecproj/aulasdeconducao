@extends('layouts.app')

@section('title')
<title>Editar Instrutor: {{ ucfirst($instructor->name) }}</title>
@endsection

@section('css')
@endsection

@section('content')
<div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form method="post" action="/instructors/{{ $instructor->id }}">
                        {{ method_field('PUT') }}
                        @include('instructors.form')
                        
                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Categoria</label>

                            <div class="col-md-6">
                                <select name="category_id[]" multiple size="6">
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ $categorysarray->contains('category_id', $category->id) ? 'selected' : '' }}>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <input class="btn btn-xs btn-primary" type="submit" value="Submeter Alterações">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
@endsection