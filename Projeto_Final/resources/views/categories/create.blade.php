@extends('layouts.app')

@section('title')
<title>Criar Categoria</title>
@endsection

@section('css')
@endsection

@section('content')
<div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/categories">
                        @include('categories.form')
                        <div class="form-group">
                            <div class="col-md-6">
                                <input class="btn btn-xs btn-primary" type="submit" value="Submeter">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
@endsection