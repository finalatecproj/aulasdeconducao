@extends('layouts.app')

@section('title')
<title>Editar Categoria: {{ ucfirst($category->name) }}</title>
@endsection

@section('css')
@endsection

@section('content')
<div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form method="post" action="/categories/{{ $category->id }}">
                        @include('categories.form')
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="col-md-6">
                                <input class="btn btn-xs btn-primary" type="submit" value="Submeter Alterações">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
@endsection
