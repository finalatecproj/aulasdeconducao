@extends('layouts.app')

@section('title')
<title>Categorias</title>
@endsection

@section('css')
@endsection

@section('content')
    <p>
        <a href="/categories/create" class="btn btn-success">Criar Categoria</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">Lista de todas as Categorias</div>

        <div class="panel-body table-responsive">
        {{--  remove datatable if dont want the entries and other stuff  --}}
            <table id="datatable" class="table table-bordered table-striped {{ count($categories) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($categories) > 0)
                        @foreach ($categories as $category)
                            <tr data-entry-id="{{ $category->id }}">
                                <td>{{ ucfirst($category->name) }}</td>
                                <td>
                                    <a href="/categories/{{ $category->id }}" class="btn btn-xs btn-primary">Ver</a>
                                    <a href="/categories/{{ $category->id }}/edit" class="btn btn-xs btn-info">Editar</a>
                                    <form method="post" action="/categories/{{ $category->id}}" style="display: inline-block;">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <input type="submit" class="btn btn-xs btn-danger" value="Apagar">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">Não existem categorias</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('javascript')
@endsection