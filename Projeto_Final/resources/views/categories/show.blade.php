@extends('layouts.app')

@section('title')
<title>{{ $category->id }} - {{ ucfirst($category->name) }}</title>
@endsection

@section('css')
@endsection

@section('content')
<div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>
                        <b>Nome</b>
                        {{ ucfirst($category->name) }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection