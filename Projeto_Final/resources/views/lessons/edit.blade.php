@extends('layouts.app')

@section('title')
<title>Editar Aula</title>
@endsection

@section('css')
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css'/>
<link rel="stylesheet" href="{{ url('css') }}/components.css"/>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
<div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/lessons/{{ $lesson->id }}">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                @if(Session::has('24hourserror'))
                                    <span class="help-block text-center">
                                        <strong>{{ Session::get('24hourserror') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-12">
                                @if(Session::has('lesson_marked'))
                                    <span class="help-block text-center">
                                        <strong>{{ Session::get('lesson_marked') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}">
                        <label for="start_time" class="col-md-4 control-label">Hora de Inicio</label>

                            <div class="col-md-6">
                                <input id="start_time" type="text" class="form-control datetime" name="start_time" value="{{ old('start_time')?:$lesson->start_time }}" required>

                                @if ($errors->has('start_time'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('start_time') }}</strong>
                                    </span>
                                @endif
                                @if(Session::has('timeError'))
                                    <span class="help-block">
                                        <strong>{{ Session::get('timeError') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <input type="text" class="hidden" name="student_id"  value="{{ $lesson->student_id}}" disable>

                        <div class="form-group">
                            <div class="col-md-6">
                                <input class="btn btn-xs btn-primary" type="submit" value="Submeter Alterações">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection

@section('javascript')
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
<script src="{{ url('js') }}/bootstrap.min.js"></script>
<script src="{{ url('js') }}/select2.full.min.js"></script>
<script src="{{ url('js') }}/main.js"></script>

<script src="{{ url('js') }}/timepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script>
    $('.datetime').datetimepicker({
        beforeShowDay: function(date) {
            var day = date.getDay();
            return [(day != 0), ''];
        },
        autoclose: true,
        dateFormat: "{{ config('app.date_format_js') }}",
        timeFormat: "HH:mm:ss",
        minDate: 0,
        maxDate: "+1m",
        showButtonPanel: false
    });
</script>
@endsection