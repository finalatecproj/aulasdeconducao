@extends('layouts.app')

@section('title')
<title>Criar Aula</title>
@endsection

@section('css')
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
@endsection

@section('content')
    @if(Auth::User()->is_admin == 0 && $student->state == 1)
        <p>
            <a href="{{ route('lessons.create') }}"
               class="btn btn-success">Criar Aula</a>

        </p>
    @endif

    <div class="row">
        <div class="col-md-12">
            @if(Session::has('erroDia'))
                <span class="help-block text-center">
                    <strong>{{ Session::get('erroDia') }}</strong>
                </span>
            @endif
        </div>
    </div>

@if($student->state == 1 || Auth::User()->is_admin == 1)
<div id='calendar'></div>
@endif


<div class="panel panel-default">
        <div class="panel-heading">Lista de Todas as Aulas</div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($lessons) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Aluno</th>
                    <th>Instrutor</th>
                    <th>Hora de Inicio</th>
                    <th>Hora do Fim</th>
                    @if($student->state == 1 || ($student->state == null && Auth::User()->is_admin == 1))
                    <th>Ações</th>
                    @endif
                </tr>
                </thead>
                
                <tbody>
                @if (count($lessons) > 0)
                    @foreach ($lessons as $lesson)
                        <tr data-entry-id="{{ $lesson->id }}">
                            <td>{{ isset($lesson->student_id) ? $lesson->student->name : '' }}</td>
                            <td>{{ isset($lesson->instructor_id) ? $lesson->instructor->name : '' }}</td>
                            <td>{{ $lesson->start_time }}</td>
                            <td>{{ $lesson->finish_time }}</td>
                            @if($student->state == 1 || ($student->state == null && Auth::User()->is_admin == 1))
                            <td>
                                    <a href="{{ route('lessons.edit',[$lesson->id]) }}"
                                       class="btn btn-xs btn-info">Editar</a>
                                @if(Auth::User()->is_admin == 1)
                                    <form method="post" action="/lessons/{{ $lesson->id}}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <input type="submit" value="Eliminar">
                                </form> 
                                @endif
                            </td>
                            @endif
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">Não existem aulas</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('javascript')
<!--script>
        @can('appointment_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.appointments.mass_destroy') }}';
        @endcan

    </script-->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    
    <script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
    <script src="{{ url('js') }}/bootstrap.min.js"></script>
    <script src="{{ url('js') }}/select2.full.min.js"></script>
    <script src="{{ url('js') }}/main.js"></script>
    <script>
        window._token = '{{ csrf_token() }}';
    </script>

    <script src="{{url('js')}}/moment.min.js"></script>
    <script src="{{ url('js') }}/fullcalendar.min.js"></script>
    <script>
        $(document).ready(function() {
            // page is now ready, initialize the calendar...
            $('#calendar').fullCalendar({
                // put your options and callbacks here
                defaultView: 'agendaWeek',
                minTime: '09:00:00',
                maxTime: '20:00:00',
                contentHeight: 603,
                events : [
                    
                    @foreach($lessons as $lesson)
                    {
                        title : `Student: {{$lesson->student->name}}` + " " + `Instructor: {{$lesson->instructor->name}}`,
                        Instructor : '{{ 'Instructor:' . $lesson->instructor->name}}',
                        start : '{{ $lesson->start_time }}',
                        @if ($lesson->finish_time)
                            end: '{{ $lesson->finish_time }}',
                        @endif
                        url : '{{ route('lessons.edit', $lesson->id) }}'
                        
                    },
                    @endforeach
                ]
            })
        });
</script>
@endsection