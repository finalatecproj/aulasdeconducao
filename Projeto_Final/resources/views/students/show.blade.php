@extends('layouts.app')

@section('title')
<title>{{ $student->id }} - {{ ucfirst($student->name) }}</title>
@endsection

@section('css')
@endsection

@section('content')
<div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>
                        <b>Nome</b>
                        {{ ucfirst($student->name) }}
                    </p>
                    <p>
                        <b>Email</b>
                        {{ $student->email }}
                    </p>
                    <p>
                        <b>Numero de Telefone</b>
                        {{ $student->phone }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
