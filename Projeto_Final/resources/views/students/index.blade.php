@extends('layouts.app')

@section('title')
<title>Alunos</title>
@endsection

@section('css')
@endsection

@section('content')
    <p>
        <a href="/students/create" class="btn btn-success">Criar Aluno</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">Lista de todos os Alunos</div>

        <div class="panel-body table-responsive">
        {{--  remove datatable if dont want the entries and other stuff  --}}
            <table id="datatable" class="table table-bordered table-striped {{ count($students) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Numero de Telefone</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($students) > 0)
                        @foreach ($students as $student)
                            <tr data-entry-id="{{ $student->id }}">
                                <td>{{ ucfirst($student->name) }}</td>
                                <td>{{ $student->email }}</td>
                                <td>{{ $student->phone }}</td>
                                <td>
                                    <a href="/students/{{ $student->id }}" class="btn btn-xs btn-primary">Ver</a>
                                    <a href="/students/{{ $student->id }}/edit" class="btn btn-xs btn-info">Editar</a>
                                    <form method="post" action="/students/{{ $student->id}}" style="display: inline-block;">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <input type="submit" class="btn btn-xs btn-danger" value="Apagar">
                                    </form>
                                    @if($student->state==1)
                                        <a href="/students/unestado/{{$student->id}}" style="text-decoration:none;">Desativar Aluno &nbsp;<i class="fa fa-check text-center"></i></a>
                                    @else 
                                        <a href="/students/estado/{{$student->id}}" style="text-decoration:none;">Ativar Aluno &nbsp;<i class="fa fa-times text-center"></i>  </a>                                   
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">Não existem alunos</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection