<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="{{ url('css2') }}/bootstrap.min.css"/>
<link rel="stylesheet" href="myfirstpage/layout/css/style.css">
<link href="{{ asset('css2/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ url('css2') }}/components.css"/>
<link rel="stylesheet" href="{{ url('css2') }}/quickadmin-layout.css"/>
<link rel="stylesheet" href="{{ url('css2') }}/quickadmin-theme-default.css"/>
<link rel="stylesheet" href="{{ url('css2') }}/select2.min.css"/>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
</head>

<body class="page-header-fixed">
    <div class="clearfix"></div>
    <div class="page-container">
    </div>
        <div class="container page-content-wrapper">
            <div style="margin-left: 0px;" class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2" style="text-align: center;">
                                                <span>Login - body not centered talk to joao</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3" style="text-align: center;">
                                                <span>O pedido de reset de password foi bem sucessido, caso queira prosseguir com a ação e alterar a sua palavra-passe clique no botão!</span>
                                                <br/>
                                                <span>Aviso! No caso de não ter feito esta ação ignore este email.</span>
                                                <br/>
                                                <a href="" class="btn btn-xs btn-info" target="_blank">Recuperar Password</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://use.fontawesome.com/9685d1845f.js"></script>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
    <script src="{{ url('js') }}/bootstrap.min.js"></script>
    <script src="{{ url('js') }}/select2.full.min.js"></script>
    <script src="{{ url('js') }}/main.js"></script>
</body>
</html>