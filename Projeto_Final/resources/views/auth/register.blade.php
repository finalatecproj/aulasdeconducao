@extends('layouts.app')

@section('title')
    <title>Registar Utilizadores</title>
@endsection

@section('css')
    <!-- uncomment this to see '/' css -->
 	<!-- Animate.css -->
     {{--  <link rel="stylesheet" href="myfirstpage/layout/css/animate.css">
	<!-- Flexslider -->
	<link rel="stylesheet" href="myfirstpage/layout/css/flexslider.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="myfirstpage/layout/css/icomoon.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="myfirstpage/layout/css/magnific-popup.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="myfirstpage/layout/css/bootstrap.css">

	<!--Default Theme Style You can change the style.css (default color purple) to one of these styles-->
	<link rel="stylesheet" href="myfirstpage/layout/css/style.css">  --}}
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registo</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('student_id') ? ' has-error' : '' }}">
                            <label for="student_id" class="col-md-4 control-label">E-mail do aluno</label>

                            <div class="col-md-6">
                                <select class="form-control" name="student_id" required>
                                    <option value="" disabled selected>Selecione o Email</option>
                                    @foreach($students as $student)
                                        <option value="{{ $student->id }}">{{$student->email}}</option>
                                    @endforeach
                                </select>
                                @if(Session::has('student_id'))
                                    <span class="help-block">
                                        <strong>{{ Session::get('student_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <button 
                            type="button" 
                            class="btn btn-primary btn-lg"
                            
                                data-toggle="modal" 
                                data-target="#favoritesModal"
                            >
                            Registar
                        </button>

                        <div class="modal fade" id="favoritesModal" 
                            tabindex="-1" role="dialog" 
                            aria-labelledby="favoritesModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="favoritesModalLabel">Atenção:</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                        Pretende criar mais users?
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-default" name="cont" value="0">No</button>
                                        <span class="pull-right">
                                            <button type="submit" class="btn btn-primary" name="cont" value="1">Yes</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<footer id="fh5co-footer" style="
    bottom: 0px;
    position: fixed;
    left: 0px;
    right: 0px;">
			<div class="container">
				<div class="row row-bottom-padded-md">
					<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
						<div class="fh5co-footer-widget">
							<h3>Company</h3>
							<ul class="fh5co-links">
								<li><a href="#">About Us</a></li>
								<li><a href="#">Careers</a></li>
								<li><a href="#">Feature Tour</a></li>
								<li><a href="#">Pricing</a></li>
								<li><a href="#">Team</a></li>
							</ul>
						</div>
					</div>

					<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
						<div class="fh5co-footer-widget">
							<h3>Support</h3>
							<ul class="fh5co-links">
								<li><a href="#">Knowledge Base</a></li>
								<li><a href="#">24/7 Call Support</a></li>
								<li><a href="#">Video Demos</a></li>
								<li><a href="#">Terms of Use</a></li>
								<li><a href="#">Privacy Policy</a></li>
							</ul>
						</div>
					</div>

					<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
						<div class="fh5co-footer-widget">
							<h3>Contact Us</h3>
							<p>
								<a href="mailto:info@freehtml5.co">info@freehtml5.co</a> <br>
								198 West 21th Street, <br>
								Suite 721 New York NY 10016 <br>
								<a href="tel:+123456789">+12 34  5677 89</a>
							</p>
						</div>
					</div>
				</div>
                
			</div>
			<div class="fh5co-copyright animate-box">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<p class="fh5co-left"><small>&copy; 2016 <a href="index.html">Guide</a> free html5. All Rights Reserved.</small></p>
							<p class="fh5co-right"><small class="fh5co-right">Designed by <a href="http://freehtml5.co" target="_blank">FREEHTML5.co</a> Demo Images: <a href="http://unsplash.com" target="_blank">Unsplash</a></small></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
@endsection