<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

@yield('title')

@yield('css')

<!-- Styles -->
<link href="{{ asset('css2/app.css') }}" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{ url('css2') }}/bootstrap.min.css"/>
<link rel="stylesheet" href="{{ url('css2') }}/components.css"/>
<link rel="stylesheet" href="{{ url('css2') }}/quickadmin-layout.css"/>
<link rel="stylesheet" href="{{ url('css2') }}/quickadmin-theme-default.css"/>
<link rel="stylesheet" href="{{ url('css2') }}/select2.min.css"/>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css"/>
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.standalone.min.css"/>
</head>

<body class="page-header-fixed">
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner">
            <div class="page-header-inner">
                <div class="navbar-header">
                @if(Auth::Guest())
                    <a href="{{ url('/') }}" class="navbar-brand">Home</a>
                @elseif(!Auth::Guest() && Auth::User()->is_admin == 1)
                    <a href="{{ url('/home') }}" class="navbar-brand">Back-Office</a>
                @else
                    <a href="{{ url('/home') }}" class="navbar-brand">Home</a>
                @endif
                </div>
                @if(Route::currentRouteName() != 'password.request' && Route::currentRouteName() != 'login')
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse" aria-expended="false"></a>
                @endif
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="page-container">
        @if(Auth::Guest())
        @elseif(!Auth::Guest() && Auth::User()->is_admin == 1)
            <div class="page-sidebar-wrapper">
            @inject('request', 'Illuminate\Http\Request')
                <div class="page-sidebar-wrapper">
                    <div class="page-sidebar navbar-collapse collapse" aria-expanded="false">
                        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                            <li class="{{ $request->segment(1) == 'home' && $request->segment(2) == null ? 'active' : '' }}">
                                <a href="{{ route('home') }}">
                                    <i class="fa fa-home"></i>
                                    <span class="title">Home</span>
                                </a>
                            </li>
                            <li class="{{ $request->segment(1) == 'categories' ? 'active' : '' }}">
                                <a href="/categories">
                                    <i class="fa fa-wrench"></i>
                                    <span class="title">Categorias</span>
                                </a>
                            </li>
                            <li class="{{ $request->segment(1) == 'students' ? 'active' : '' }}">
                                <a href="/students">
                                    <i class="fa fa-users"></i>
                                    <span class="title">Alunos</span>
                                </a>
                            </li>
                            <li class="{{ $request->segment(1) == 'instructors' ? 'active' : '' }}">
                                <a href="/instructors">
                                    <i class="fa fa-user"></i>
                                    <span class="title">Instrutores</span>
                                </a>
                            </li>
                            <li class="{{ $request->segment(1) == 'register' ? 'active' : '' }}">
                                <a href="{{ route('register') }}">
                                    <i class="fa fa-address-card-o"></i>
                                    <span class="title">Criar Utilizadores</span>
                                </a>
                            </li>
                            <li class="{{ $request->segment(1) == 'vehicles' ? 'active' : '' }}">
                                <a href="/vehicles">
                                    <i class="fa fa-car"></i>
                                    <span class="title">Veículos</span>
                                </a>
                            </li>
                            <li class="{{ $request->segment(1) == 'lessons' ? 'active' : '' }}">
                                <a href="/lessons">
                                    <i class="fa fa-calendar"></i>
                                    <span class="title">Aulas</span>
                                </a>
                            </li>
                            <li class="{{ $request->segment(1) == 'home' && $request->segment(2) == 'passwordreset' ? 'active' : '' }}">
                                <a href="/home/passwordreset">
                                    <i class="fa fa-key"></i>
                                    <span class="title">Alterar Password</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off"></i>
                                    <span class="title">Logout</span>                                
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
        @else
            <div class="page-sidebar-wrapper">
            @inject('request', 'Illuminate\Http\Request')
                <div class="page-sidebar-wrapper">
                    <div class="page-sidebar navbar-collapse collapse" aria-expanded="true">
                        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                            <li class="{{ $request->segment(1) == 'home' && $request->segment(2) == null ? 'active' : '' }}">
                                <a href="{{ route('home') }}">
                                    <i class="fa fa-home"></i>
                                    <span class="title">Home</span>
                                </a>
                            </li>
                            <li class="{{ $request->segment(1) == 'lessons' ? 'active' : '' }}">
                                <a href="/lessons">
                                    <i class="fa fa-calendar"></i>
                                    <span class="title">Aulas</span>
                                </a>
                            </li>
                            <li class="{{ $request->segment(1) == 'home' && $request->segment(2) == 'passwordreset' ? 'active' : '' }}">
                                <a href="/home/passwordreset">
                                    <i class="fa fa-key"></i>
                                    <span class="title">Alterar Password</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off"></i>
                                    <span class="title">Logout</span>                                
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
        @endif
    </div>
    
        <div class="container page-content-wrapper">
            @if(Route::currentRouteName() != 'login' && Route::currentRouteName() != 'password.request' && Route::currentRouteName() != 'password.reset')
            <div class="page-content">
            @else
            <div style="margin-left: 0px;" class="page-content">
            @endif
                @if(isset($siteTitle))
                    <h3 class="page-title">
                        {{ $siteTitle }}
                    </h3>
                @endif

                <div class="row">
                    <div class="col-md-12">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="scroll-to-top"
         style="display: none;">
        <i class="fa fa-arrow-up"></i>
    </div>

    <script src="https://use.fontawesome.com/9685d1845f.js"></script>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
    <script src="{{ url('js') }}/bootstrap.min.js"></script>
    <script src="{{ url('js') }}/select2.full.min.js"></script>
    <script src="{{ url('js') }}/main.js"></script>
    <script>
        window._token = '{{ csrf_token() }}';
    </script>

    @yield('javascript')
</body>
</html>