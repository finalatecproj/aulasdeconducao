<?php

namespace App\Http\Controllers;

use App\Instructor;
use App\Student;
use App\Vehicle;
use App\Lesson;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;

class XamarinController extends Controller
{
    /*
    *1-A hora a que está a tentar inserir é inválida
    +2-Não é possivel marcar uma aula nesta hora!
    *3-Aula criada com sucesso
    */
    public function createLesson(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_time' => 'required|date_format:"Y-m-d H:i:s"',
            'student_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }

        $time = Carbon::parse($request->get('start_time'));
        if($time->hour <= 8 && $time->hour >= 20 || $time->minute != 0 || $time->second != 0)
        {
            return 1;
        }
        
        $instructors = Instructor::All();
        $students = Student::All();
        $vehicles = Vehicle::All();
        $timeRequested = $request->get('start_time');
        $arrayIns = [];
        $arrayVehi = [];
        $arrayStu = [];

        if(Lesson::where('start_time','=', $timeRequested)->count() > 0)
        {
            $LessonsOnThisDay = Lesson::where('start_time','=', $timeRequested)->get();

            foreach ($instructors as $instructor)
            {
                if(!$LessonsOnThisDay->contains('instructor_id', $instructor->id))
                {
                    $arrayIns[]+=$instructor->id;
                }
            }
            
            foreach ($vehicles as $vehicle)
            {
                if(!$LessonsOnThisDay->contains('vehicle_id', $vehicle->id))
                {
                    $arrayVehi[]+=$vehicle->id;
                }
            }

            foreach ($students as $student)
            {
                if(!$LessonsOnThisDay->contains('student_id', $student->id))
                {
                    $arrayStu[]+=$student->id;
                }
            }

            if(!empty($arrayIns) && !empty($arrayVehi) && in_array($request->get('student_id'), $arrayStu))
            {
                $lesson = new Lesson();
                $lesson->vehicle_id = array_random($arrayVehi);
                $lesson->instructor_id = array_random($arrayIns);
                $lesson->student_id = $request->get('student_id');
                $lesson->start_time = $timeRequested;
                $lesson->finish_time = Carbon::parse($request->get('start_time'))->addHour(1);
                $lesson->save();
            }
            else
            {
                return 2;
            }
        }
        else
        {
            $lesson = new Lesson();
            $lesson->vehicle_id = array_random($vehicles->toArray())['id'];
            $lesson->instructor_id = array_random($instructors->toArray())['id'];
            $lesson->student_id = $request->get('student_id');
            $lesson->start_time = $timeRequested;
            $lesson->finish_time = Carbon::parse($request->get('start_time'))->addHour(1);
            $lesson->save();
        }

        return 3;
    }

    /*
    *1-A hora a que está a tentar inserir é inválida
    +2-Aula modificada com sucesso
    *3-Não é possivel marcar uma aula nesta hora!
    *4-Aula modificada com sucesso
    *5-Aula não existente!
    */
    public function updateLesson(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'start_time' => 'required|date_format:"Y-m-d H:i:s"',
            'student_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }

        $tryFindingLesson = Lesson::find($id);

        if($tryFindingLesson)
        {
            $time = Carbon::parse($request->get('start_time'));
        
            if($time->hour <= 8 && $time->hour >= 20 || $time->minute != 0 || $time->second != 0)
            {
                return 1;
            }
        
            $instructors = Instructor::All();
            $students = Student::All();
            $vehicles = Vehicle::All();
            $timeRequested = $request->get('start_time');
            $arrayIns = [];
            $arrayVehi = [];
            $arrayStu = [];
      
            if(Lesson::where('start_time','=', $timeRequested)->count() > 0)
            {
                $LessonsOnThisDay = Lesson::where('start_time','=', $timeRequested)->get();
            
                foreach ($instructors as $instructor)
                {
                    if(!$LessonsOnThisDay->contains('instructor_id', $instructor->id))
                    {
                        $arrayIns[]+=$instructor->id;
                    }
                }
                    
                foreach ($vehicles as $vehicle)
                {
                    if(!$LessonsOnThisDay->contains('vehicle_id', $vehicle->id))
                    {
                        $arrayVehi[]+=$vehicle->id;
                    }
                }
            
                foreach ($students as $student)
                {
                    if(!$LessonsOnThisDay->contains('student_id', $student->id))
                    {
                        $arrayStu[]+=$student->id;
                    }
                }
            
                if(!empty($arrayIns) && !empty($arrayVehi) && in_array($request->get('student_id'), $arrayStu))
                {
                    $lesson = new Lesson();
                    $lesson->vehicle_id = array_random($arrayVehi);
                    $lesson->instructor_id = array_random($arrayIns);
                    $lesson->student_id = $request->get('student_id');
                    $lesson->start_time = $timeRequested;
                    $lesson->finish_time = Carbon::parse($request->get('start_time'))->addHour(1);
                    $lesson->save();
            
                    $lesson2 = Lesson::findOrFail($id);
                    
                    $lesson2->delete();
                    
                    return 2;
                }
                else
                {
                    return 3;
                }
            }
            else
            {
                $lesson = new Lesson();
                $lesson->vehicle_id = array_random($vehicles->toArray())['id'];
                $lesson->instructor_id = array_random($instructors->toArray())['id'];
                $lesson->student_id = $request->get('student_id');
                $lesson->start_time = $timeRequested;
                $lesson->finish_time = Carbon::parse($request->get('start_time'))->addHour(1);
                $lesson->save();
                $lesson2 = Lesson::findOrFail($id);
            
                $lesson2->delete();

                return 4;
            }
        }

        return 5;
    }

    //return lessons only from student id
    //public function showAllByDayLesson($date, $student_id) //yyyy-mm-dd
    public function showAllByDayLesson($date) //yyyy-mm-dd
    {
        //dunno how to do this one
        //dunno wat params to get for this
        /*$validator = Validator::make($request->all(), [
            'date' => 'required|date_format:"Y-m-d"',
            'student_id' => 'required'
        ]);
        
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }*/

        //$lessons = Lesson::where('start_time', 'like', $request->get('date') . '%')->get();
        //$lessons = Lesson::where('start_time', 'like', $date . '%')->where('student_id', '=', $student_id)->get();
        $lessons = Lesson::where('start_time', 'like', $date . '%')->get();
        if($lessons->count() > 0)
        {
            return $lessons;
        }    

        return 0;
    }

    public function showOneLesson($id , $student_id)
    {
        $lesson = Lesson::find($id);
        
        if($lesson->student_id != $student_id)
        {
            return 0;
        }

        if($lesson) {
            return $lesson;
        }

        return 0;
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }

        $email= $request->get('email');
        $password= $request->get('password');

        if (Auth::attempt(['email' => $email, 'password' => $password])) {

            $user = User::where('email', '=', $request->get('email'))->first()->student_id;

            if($user != null)
            {
                return $user;
            }
            else
            {
                return -1; //to know that he is an admin
            }
        }

        //false
        return 0;
    }

    /*
    * turn month number to month name
    *
    */
    private function monthName($monthNumber)
    {
        switch ($monthNumber) {
            case '2':
                return 'Fevereiro';
            case '3':
                return 'Março';
            case '4':
                return 'Abril';
            case '5':
                return 'Maio';
            case '6':
                return 'Junho';
            case '7':
                return 'Julho';
            case '8':
                return 'Agosto';
            case '9':
                return 'Setembro';
            case '10':
                return 'Outubro';
            case '11':
                return 'Novembro';
            case '12':
                return 'Dezembro';
            default:
                return 'Janeiro';
        }
    }
}