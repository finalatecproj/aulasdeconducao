<?php

namespace App\Http\Controllers;

use App\CategoryInstructor;
use Illuminate\Http\Request;

class CategoryInstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CategoryInstructor  $categoryInstructor
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryInstructor $categoryInstructor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CategoryInstructor  $categoryInstructor
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryInstructor $categoryInstructor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoryInstructor  $categoryInstructor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoryInstructor $categoryInstructor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CategoryInstructor  $categoryInstructor
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryInstructor $categoryInstructor)
    {
        //
    }
}
