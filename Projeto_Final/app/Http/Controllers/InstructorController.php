<?php

namespace App\Http\Controllers;

use App\Category;
use App\Student;
use App\Instructor;
use App\Lesson;
use Carbon\Carbon;
use App\CategoryInstructor;
use Illuminate\Http\Request;

class InstructorController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $instructors = Instructor::all();
        return view('instructors.index')
            ->with(compact('instructors'));
    }
 
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $instructor = new Instructor();
        $categories = Category::All();
        return view('instructors.create')
            ->with(compact('categories','instructor'));
    }
 
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
         
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'category_id' => 'required'
        ]);
        
        $instructor = new Instructor();
        $instructor->name = $request->get('name');
        $instructor->phone = $request->get('phone');
        $instructor->email = $request->get('email');
        $instructor->save();
        $instructor->categories()->attach($request->get('category_id'));

        return redirect('/instructors');
    }
 
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //devemos retornar para o index caso não encontre o id
        $instructor = Instructor::findOrFail($id);
        return view('instructors.show')
            ->with(compact('instructor'));
    }
 
    /**
     * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $instructor = Instructor::findOrFail($id);
        $categorysarray = CategoryInstructor::where('instructor_id', 'like', $instructor->id)->select('category_id')->get();
        $categories = Category::All();
        return view('instructors.edit')
            ->with(compact('instructor', 'categories', 'categorysarray'));
    }
 
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'category_id' => 'nullable'
        ]);
           
        $instructor = Instructor::findOrFail($id);
        $instructor->name = $request->get('name');
        $instructor->phone = $request->get('phone');
        $instructor->email = $request->get('email');
        $instructor->save();

        CategoryInstructor::where('instructor_id','=', $instructor->id)->delete();

        if($request->get('category_id') != null)
        {
            $instructor->categories()->attach($request->get('category_id'));
        }
        
        return redirect('/instructors');
    }
 
     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         $instructor = Instructor::findOrFail($id);
         $today=Carbon::now();

        
        $lessons = Lesson::where('instructor_id','=',$instructor->id)->where('start_time','>=',$today->year . '-' . $today->month . '-' . $today->day . '%')->get();

        if($lessons->count() != 0)
        {
            foreach ($lessons as $lesson) {
                $lessontime = Carbon::parse($lesson->start_time);
                $day = $lessontime->day;
                $month = self::monthName($lessontime->month);
                $hour = $lessontime->hour;
                $email = Student::where('id','=', $lesson->student_id)->first()->email;
                \Mail::send('emails.desmarcada', ['day' => $day, 'month' => $month, 'hour' => $hour], function ($message) use ($email)
                {
                    $message->to($email);
                });
             }
        }
         
         $instructor->delete();
         return redirect('/instructors');
     }

     /*
    * turn month number to month name
    *
    */
    private function monthName($monthNumber)
    {
        switch ($monthNumber) {
            case '2':
                return 'Fevereiro';
            case '3':
                return 'Março';
            case '4':
                return 'Abril';
            case '5':
                return 'Maio';
            case '6':
                return 'Junho';
            case '7':
                return 'Julho';
            case '8':
                return 'Agosto';
            case '9':
                return 'Setembro';
            case '10':
                return 'Outubro';
            case '11':
                return 'Novembro';
            case '12':
                return 'Dezembro';
            default:
                return 'Janeiro';
        }
    }
}
