<?php

namespace App\Http\Controllers;
use App\Student;
use App\Category;
use App\Lesson;
use App\CategoryStudent;
use Illuminate\Http\Request;

class StudentController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         $students = Student::all();
         return view('students.index')
             ->with(compact('students'));
     }
 
     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
        $student = new Student();
        $categories = Category::All();
        return view('students.create')
            ->with(compact('student', 'categories'));
     }
 
     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
         $request->validate([
             'name' => 'required',
             'email' => 'required|email',
             'phone' => 'required|numeric|digits:9',
             'category_id' => 'required'
         ]);
         
         $student = new Student();
         $student->name = $request->get('name');
         $student->phone = $request->get('phone');
         $student->email = $request->get('email');
         $student->state = 1;
         $student->save();
         $student->categories()->attach($request->get('category_id'));
         return redirect('/students');
     }
 
     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //devemos retornar para o index caso não encontre o id
         $student = Student::findOrFail($id);
         return view('students.show')
             ->with(compact('student'));
     }
 
    /**
     * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $categories = Category::All();
        $student = Student::findOrFail($id);
        $categorysarray = CategoryStudent::where('student_id', 'like', $student->id)->select('category_id')->get();
        return view('students.edit')
            ->with(compact('student', 'categorysarray', 'categories'));
    }
 
    /**
     * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'category_id' => 'nullable'
        ]);
        
        $student = Student::find($id);
        $student->name = $request->get('name');
        $student->phone = $request->get('phone');
        $student->email = $request->get('email');
        $student->save();

        //CategoryStudent::where('student_id','=', $student->id)->delete();
        $student->categories()->detach();

        if($request->get('category_id') != null)
        {
            $student->categories()->attach($request->get('category_id'));
        }
    
        return redirect('/students');
    }
 
    /**
     * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $student = Student::findOrFail($id);
        $lesson = Lesson::where('student_id','like',$id)->get();
        $student->delete();
        return redirect('/students');
    }

    // Altera o estado do aluno
    public function estado($id)
    {
        $student = Student::findOrFail($id);
        $student->state = 1;
        $student->save();
        return redirect(url()->previous());
    }

    public function unestado($id)
    {
        $student = Student::findOrFail($id);
        $student->state = 0;
        $student->save();
        return redirect(url()->previous());
    }
}