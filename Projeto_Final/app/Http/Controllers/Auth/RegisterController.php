<?php

namespace App\Http\Controllers\Auth;

use App\Student;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('AdminOnly');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        dd('validation');
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'student_id' => 'required'
        ]);
    }

    /**
     * Create user if email already exist return erro
     *
     */
    public function register(Request $request)
    {
        if(User::where('email', 'like', Student::where('id','=', $request->get('student_id'))->first()->email)->first())
        {
            Session::flash('student_id', 'Este email ja existe!');

            $request->validate([
                'name' => 'required|string|max:255',
                'student_id' => 'required'
            ]);

            return Redirect::back();
        }
        else
        {
            $request->validate([
                'student_id' => 'required',
                'cont' => 'required|integer|between:0,1',
            ]);

            $name = Student::where('id','=', $request->get('student_id'))->first()->name;
            $password = str_random(6);
            $email = Student::where('id','=', $request->get('student_id'))->first()->email;

            $user = new User();
            $user->name = $name;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->student_id = $request->get('student_id');
            $user->save();

            \Mail::send('emails.sendpassword', ['password' => $password,'name' => $name,'email' => $email], function ($message) use ($email)
            {
                $message->to($email);
            });

            if($request->get('cont') == 1)
            {
                return Redirect::back();
            }
            else
            {
                //change this redirect
                return redirect('/home');
            }
        }
    }

    /**
    * Show the application registration form.
    *
    * @return \Illuminate\Http\Response
    */
    public function showRegistrationForm()
    {
        $students = Student::All();
        return view('auth.register')->with(compact('students'));
    }
}
