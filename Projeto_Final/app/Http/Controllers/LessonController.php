<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Student;
use App\Instructor;
use App\Vehicle;
use App\Lesson;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
        if(Auth::user() && Auth::user()->is_admin == 1)
        {
            $lessons = Lesson::where('start_time','>=', Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day . '%' )->get();
            $student = new Student();
            return view('lessons.index')
            ->with(compact('lessons','student'));
        }
        elseif (Auth::user() && Auth::user()->is_admin == 0)
        {
            $student = Student::where('id','=',Auth::user()->student_id)->first();
            $lessons = Lesson::where('student_id','like', Auth::User()->student_id)->get();
            return view('lessons.index')
            ->with(compact('lessons', 'student'));
            
        }
         
         
     }
 
     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         return view('lessons.create');
     }
 
     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
      public function store(Request $request)
      {

        $request->validate([
              'start_time' => 'required|date_format:"Y-m-d H:i:s"'
        ]);

        $time = Carbon::parse($request->get('start_time'));
        if($time->hour <= 8 && $time->hour >= 20 || $time->minute != 0 || $time->second != 0)
        {
            Session::flash('timeError', 'A hora a que está a tentar inserir é inválida!');
            return Redirect::back();
        }
        
        $timeRequested = $request->get('start_time');
        $year = $time->year;
        $month = $time->month;
        $day = $time->day;
        if(Lesson::where('student_id','=', Auth::User()->student_id)->where('start_time','like', $year . '-' . $month . '-' . $day . '%')->count() < 3)
        {
            $instructors = Instructor::All();
            $students = Student::All();
            $vehicles = Vehicle::All();
            $arrayIns = [];
            $arrayVehi = [];
            $arrayStu = [];
    
            if(Lesson::where('start_time','=', $timeRequested)->count() > 0)
            {
                $LessonsOnThisDay = Lesson::where('start_time','=', $timeRequested)->get();
    
                foreach ($instructors as $instructor)
                {
                    if(!$LessonsOnThisDay->contains('instructor_id', $instructor->id))
                    {
                        $arrayIns[]+=$instructor->id;
                    }
                }
                
                foreach ($vehicles as $vehicle)
                {
                    if(!$LessonsOnThisDay->contains('vehicle_id', $vehicle->id))
                    {
                        $arrayVehi[]+=$vehicle->id;
                    }
                }
    
                foreach ($students as $student)
                {
                    if(!$LessonsOnThisDay->contains('student_id', $student->id))
                    {
                        $arrayStu[]+=$student->id;
                    }
                }
    
                if(!empty($arrayIns) && !empty($arrayVehi) && in_array(Auth::User()->student_id, $arrayStu))
                {
                    $lesson = new Lesson();
                    $lesson->vehicle_id = array_random($arrayVehi);
                    $lesson->instructor_id = array_random($arrayIns);
                    $lesson->student_id = Auth::User()->student_id;
                    $lesson->start_time = $timeRequested;
                    $lesson->finish_time = $time->addHour(1);
                    $lesson->save();
                }
                else
                {
                    Session::flash('lesson_marked', 'Não é possivel marcar uma aula nesta hora!');
                    return Redirect::back();
                }
            }
            else
            {
                $lesson = new Lesson();
                $lesson->vehicle_id = array_random($vehicles->toArray())['id'];
                $lesson->instructor_id = array_random($instructors->toArray())['id'];
                $lesson->student_id = Auth::User()->student_id;
                $lesson->start_time = $timeRequested;
                $lesson->finish_time = $time->addHour(1);
                $lesson->save();
            }
        }
        else
        {
            Session::flash('cant3plus', 'Nao é possivel marcar mais de 3 aulas por dia');
            return Redirect::back();
        }

        return redirect('/lessons');
     }
 
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //devemos retornar para o index caso não encontre o id
        $lesson = Lesson::findOrFail($id);
        return view('lessons.show')
            ->with(compact('lesson'));
    }
 
    /**
     * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {  
        $lesson = Lesson::findOrFail($id);
        if(Auth::User()->is_admin != 1 && $lesson->student_id != Auth::User()->student_id)
        {
            abort(404);
        }

        return view('lessons.edit')
            ->with(compact('lesson'));
    }

    /**
     * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $request->validate([
        'start_time' => 'required|date_format:"Y-m-d H:i:s"',
        'student_id' => 'required'
        ]);
        
        $time = Carbon::parse($request->get('start_time'));
        
        if($time->hour <= 8 && $time->hour >= 20 || $time->minute != 0 || $time->second != 0)
        {
            Session::flash('timeError', 'A hora a que está a tentar inserir é inválida!');
            return Redirect::back();
        }
        
        $timeRequested = $request->get('start_time');

        $lesson2 = Lesson::findOrFail($id);
        $third = Carbon::parse($lesson2->start_time)->subDays(1);//verificação de 24h
        if($lesson2->finish_time >= Carbon::now())
        {
            if(Auth::user()->is_admin == 1)
            {
                $instructors = Instructor::All();
                $students = Student::All();
                $vehicles = Vehicle::All();
        
                $arrayIns = [];
                $arrayVehi = [];
                $arrayStu = [];
        
                if(Lesson::where('start_time','=', $timeRequested)->count() > 0)
                {
                    $LessonsOnThisDay = Lesson::where('start_time','=', $timeRequested)->get();
        
                    foreach ($instructors as $instructor)
                    {
                        if(!$LessonsOnThisDay->contains('instructor_id', $instructor->id))
                        {
                            $arrayIns[]+=$instructor->id;
                        }
                    }
                    
                    foreach ($vehicles as $vehicle)
                    {
                        if(!$LessonsOnThisDay->contains('vehicle_id', $vehicle->id))
                        {
                            $arrayVehi[]+=$vehicle->id;
                        }
                    }
        
                    foreach ($students as $student)
                    {
                        if(!$LessonsOnThisDay->contains('student_id', $student->id))
                        {
                            $arrayStu[]+=$student->id;
                        }
                    }
        
                    if(!empty($arrayIns) && !empty($arrayVehi) && in_array($request->get('student_id'), $arrayStu))
                    {
                        $lesson = new Lesson();
                        $lesson->vehicle_id = array_random($arrayVehi);
                        $lesson->instructor_id = array_random($arrayIns);
                        if(Auth::User()->is_admin == 1)
                        {
                            $lesson->student_id = $request->get('student_id');
                        }
                        else
                        {
                            $lesson->student_id = Auth::User()->student_id;
                        }
                        $lesson->start_time = $timeRequested;
                        $lesson->finish_time = Carbon::parse($request->get('start_time'))->addHour(1);
                        $lesson->save();
        
                        $lessontime = Carbon::parse($lesson2->start_time);
                        $day = $lessontime->day;
                        $month = self::monthName($lessontime->month);
                        $hour = $lessontime->hour;
                        $actualday = $time->day;
                        $actualmonth = self::monthName($time->month);
                        $actualtime = $time->hour;
                        
                        $email = User::where('student_id', '=' , $request->get('student_id'))->first()->email;
        
                        \Mail::send('emails.movida', ['day' => $day, 'month' => $month, 'hour' => $hour, 'actualday' => $actualday, 'actualmonth' => $actualmonth, 'actualtime' => $actualtime], function ($message) use ($email)
                        {
                            $message->to($email);
                        });
                        
                        $lesson2->delete();
                        
                    }
                    else
                    {
                        Session::flash('lesson_marked', 'Não é possivel marcar uma aula nesta hora!');
                        return Redirect::back();
                    }
                }
                else
                {
                    $lesson = new Lesson();
                    $lesson->vehicle_id = array_random($vehicles->toArray())['id'];
                    $lesson->instructor_id = array_random($instructors->toArray())['id'];
                    if(Auth::User()->is_admin == 1)
                    {
                        $lesson->student_id = $request->get('student_id');
                    }
                    else
                    {
                        $lesson->student_id = Auth::User()->student_id;
                    }
                    $lesson->start_time = $timeRequested;
                    $lesson->finish_time = $time->addHour(1);
                    $lesson->save();
        
                    if(Auth::User()->is_admin == 1)
                    {
                        $lessontime = Carbon::parse($lesson2->start_time);
                        $day = $lessontime->day;
                        $month = self::monthName($lessontime->month);
                        $hour = $lessontime->hour;
                        $actualday = $time->day;
                        $actualmonth = self::monthName($time->month);
                        $actualtime = $time->hour;
        
                        $email = User::where('student_id', '=' , $request->get('student_id'))->first()->email;
                        
                        \Mail::send('emails.movida', ['day' => $day, 'month' => $month, 'hour' => $hour, 'actualday' => $actualday, 'actualmonth' => $actualmonth, 'actualtime' => $actualtime], function ($message) use ($email)
                        {
                            $message->to($email);
                        });
                    }
                    $lesson2->delete();
                }
                return redirect('/lessons');
            }
            else if(Auth::user()->is_admin == 0)
            {
                if($third >= Carbon::now())
                {
                    $instructors = Instructor::All();
        $students = Student::All();
        $vehicles = Vehicle::All();

        $arrayIns = [];
        $arrayVehi = [];
        $arrayStu = [];

        if(Lesson::where('start_time','=', $timeRequested)->count() > 0)
        {
            $LessonsOnThisDay = Lesson::where('start_time','=', $timeRequested)->get();

            foreach ($instructors as $instructor)
            {
                if(!$LessonsOnThisDay->contains('instructor_id', $instructor->id))
                {
                    $arrayIns[]+=$instructor->id;
                }
            }
            
            foreach ($vehicles as $vehicle)
            {
                if(!$LessonsOnThisDay->contains('vehicle_id', $vehicle->id))
                {
                    $arrayVehi[]+=$vehicle->id;
                }
            }

            foreach ($students as $student)
            {
                if(!$LessonsOnThisDay->contains('student_id', $student->id))
                {
                    $arrayStu[]+=$student->id;
                }
            }

            if(!empty($arrayIns) && !empty($arrayVehi) && in_array($request->get('student_id'), $arrayStu))
            {
                $lesson = new Lesson();
                $lesson->vehicle_id = array_random($arrayVehi);
                $lesson->instructor_id = array_random($arrayIns);
                if(Auth::User()->is_admin == 1)
                {
                    $lesson->student_id = $request->get('student_id');
                }
                else
                {
                    $lesson->student_id = Auth::User()->student_id;
                }
                $lesson->start_time = $timeRequested;
                $lesson->finish_time = Carbon::parse($request->get('start_time'))->addHour(1);
                $lesson->save();

                $lessontime = Carbon::parse($lesson2->start_time);
                $day = $lessontime->day;
                $month = self::monthName($lessontime->month);
                $hour = $lessontime->hour;
                $actualday = $time->day;
                $actualmonth = self::monthName($time->month);
                $actualtime = $time->hour;
                
                $email = User::where('student_id', '=' , $request->get('student_id'))->first()->email;

                \Mail::send('emails.movida', ['day' => $day, 'month' => $month, 'hour' => $hour, 'actualday' => $actualday, 'actualmonth' => $actualmonth, 'actualtime' => $actualtime], function ($message) use ($email)
                {
                    $message->to($email);
                });
                
                $lesson2->delete();
                
            }
            else
            {
                Session::flash('lesson_marked', 'Não é possivel marcar uma aula nesta hora!');
                return Redirect::back();
            }
        }
        else
        {
            $lesson = new Lesson();
            $lesson->vehicle_id = array_random($vehicles->toArray())['id'];
            $lesson->instructor_id = array_random($instructors->toArray())['id'];
            if(Auth::User()->is_admin == 1)
            {
                $lesson->student_id = $request->get('student_id');
            }
            else
            {
                $lesson->student_id = Auth::User()->student_id;
            }
            $lesson->start_time = $timeRequested;
            $lesson->finish_time = $time->addHour(1);
            $lesson->save();

            if(Auth::User()->is_admin == 1)
            {
                $lessontime = Carbon::parse($lesson2->start_time);
                $day = $lessontime->day;
                $month = self::monthName($lessontime->month);
                $hour = $lessontime->hour;
                $actualday = $time->day;
                $actualmonth = self::monthName($time->month);
                $actualtime = $time->hour;

                $email = User::where('student_id', '=' , $request->get('student_id'))->first()->email;
                
                \Mail::send('emails.movida', ['day' => $day, 'month' => $month, 'hour' => $hour, 'actualday' => $actualday, 'actualmonth' => $actualmonth, 'actualtime' => $actualtime], function ($message) use ($email)
                {
                    $message->to($email);
                });
            }
            $lesson2->delete();
        }          
                    return redirect('/lessons');
                }
                else
                {
                    Session::flash('24hourserror', 'Não é possivel alterar uma aula 24 horas antes da mesma!');
                    return Redirect::back();
                }
            }
            else
            {
                abort(404);
            }
        }
        else
        {
            Session::flash('erroDia', 'Não é possivel alterar uma aula que já foi concluida!');
            return redirect('/lessons');
        }
      
    }
 
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $lesson = Lesson::findOrFail($id);
        
        $lessontime = Carbon::parse($lesson->start_time);
        $day = $lessontime->day;
        $month = self::monthName($lessontime->month);
        $hour = $lessontime->hour;

        $email = Student::where('id','=', $lesson->student_id)->first()->email;
        \Mail::send('emails.desmarcada', ['day' => $day, 'month' => $month, 'hour' => $hour], function ($message) use ($email)
        {
            $message->to($email);
        });

        $lesson->delete();

        return redirect('/lessons');
    }

    /*
    * turn month number to month name
    *
    */
    private function monthName($monthNumber)
    {
        switch ($monthNumber) {
            case '2':
                return 'Fevereiro';
            case '3':
                return 'Março';
            case '4':
                return 'Abril';
            case '5':
                return 'Maio';
            case '6':
                return 'Junho';
            case '7':
                return 'Julho';
            case '8':
                return 'Agosto';
            case '9':
                return 'Setembro';
            case '10':
                return 'Outubro';
            case '11':
                return 'Novembro';
            case '12':
                return 'Dezembro';
            default:
                return 'Janeiro';
        }
    }

}