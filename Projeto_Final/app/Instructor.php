<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }
}
