<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function students()
    {
        return $this->belongsToMany(Student::class);
    }
    
    public function instructors()
    {
        return $this->belongsToMany(Instructor::class);
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }
}
