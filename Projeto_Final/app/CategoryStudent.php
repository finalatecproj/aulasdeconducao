<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryStudent extends Model
{
    protected $table = 'category_student';

    public function category ()
    {
        return $this->belongs(Category::class);
    }

    public function student ()
    {
        return $this->belongs(Student::class);
    }

    public function lessons ()
    {
        return $this->hasMany(Lesson::class);
    }
}