<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryInstructor extends Model
{
    protected $table = 'category_instructor';
}
