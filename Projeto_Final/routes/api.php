<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/lesson/create','XamarinController@createLesson');
Route::put('/lesson/update/{id}','XamarinController@updateLesson');
//Route::get('/lessons/{date}/{student_id}','XamarinController@showAllByDayLesson');
Route::get('/lessons/{date}','XamarinController@showAllByDayLesson');
Route::get('/lesson/{id}/{student_id}','XamarinController@showOneLesson');
Route::post('/login','XamarinController@login');