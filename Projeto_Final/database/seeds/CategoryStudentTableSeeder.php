<?php
use App\Student;
use App\Category;
use Illuminate\Database\Seeder;

class CategoryStudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_student')->insert([
            'student_id' => Student::where('name', 'like', 'Rui z')->get()->first()->id,
            'category_id' => Category::where('name', 'like', 'A')->get()->first()->id
        ]);
        DB::table('category_student')->insert([
            'student_id' => Student::where('name', 'like', 'Ruben y')->get()->first()->id,
            'category_id' => Category::where('name', 'like', 'B')->get()->first()->id
        ]);
    }
}
