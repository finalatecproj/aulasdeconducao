<?php
use App\Student;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Admin
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('atec123'),
            'is_admin' => '1'
        ]);

        //estudante
        DB::table('users')->insert([
            'name' => 'Rui z',
            'email' => 'rui@gmail.com',
            'password' => bcrypt('atec123'),
            'student_id' => Student::where('name', 'like', 'Rui z')->get()->first()->id
        ]);

        DB::table('users')->insert([
            'name' => 'Ruben y',
            'email' => 'ruben@gmail.com',
            'password' => bcrypt('atec123'),
            'student_id' => Student::where('name', 'like', 'Ruben y')->get()->first()->id
        ]);

    }
}
