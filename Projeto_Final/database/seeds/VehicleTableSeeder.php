<?php

use App\Category;
use Illuminate\Database\Seeder;

class VehicleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicles')->insert([
            'model' => 'supra',
            'year' => '1979',
            'km' => 1000,
            'brand' => 'toyata',
            'category_id' => Category::where('name', 'like', 'A')->get()->first()->id
        ]);
        DB::table('vehicles')->insert([
            'model' => 'seila2',
            'year' => '2002',
            'km' => 1000,
            'brand' => 'toyata2',
            'category_id' => Category::where('name', 'like', 'A')->get()->first()->id
        ]);
        DB::table('vehicles')->insert([
            'model' => 'jt',
            'year' => '2701',
            'km' => 1000,
            'brand' => 'suzuki',
            'category_id' => Category::where('name', 'like', 'B')->get()->first()->id
        ]);
        DB::table('vehicles')->insert([
            'model' => 'jt2',
            'year' => '2702',
            'km' => 1000,
            'brand' => 'suzuk2',
            'category_id' => Category::where('name', 'like', 'B')->get()->first()->id
        ]);
        DB::table('vehicles')->insert([
            'model' => 'seila',
            'year' => '2000',
            'km' => 1000,
            'brand' => 'toyata',
            'category_id' => Category::where('name', 'like', 'C')->get()->first()->id
        ]);
        DB::table('vehicles')->insert([
            'model' => 'seil2',
            'year' => '2002',
            'km' => 1000,
            'brand' => 'toyat2',
            'category_id' => Category::where('name', 'like', 'C')->get()->first()->id
        ]);
    }
}
