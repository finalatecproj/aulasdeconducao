<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'A'
        ]);
        DB::table('categories')->insert([
            'name' => 'B'
        ]);
        DB::table('categories')->insert([
            'name' => 'C'
        ]);
        DB::table('categories')->insert([
            'name' => 'D'
        ]);
        DB::table('categories')->insert([
            'name' => 'E'
        ]);
    }
}
