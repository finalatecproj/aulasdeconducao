<?php

use Illuminate\Database\Seeder;

class InstructorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instructors')->insert([
            'name' => 'xavier c',
            'phone' => '911432842',
            'email' => 'xavier@gmail.com'
        ]);

        DB::table('instructors')->insert([
            'name' => 'joao b',
            'phone' => '919873842',
            'email' => 'joao@gmail.com'
        ]);

        DB::table('instructors')->insert([
            'name' => 'marcelo a',
            'phone' => '911543242',
            'email' => 'marcelo@gmail.com'
        ]);
        DB::table('instructors')->insert([
            'name' => 'manuel d',
            'phone' => '919873842',
            'email' => 'manuel@gmail.com'
        ]);

        DB::table('instructors')->insert([
            'name' => 'ana e',
            'phone' => '911543242',
            'email' => 'ana@gmail.com'
        ]);
    }
}
