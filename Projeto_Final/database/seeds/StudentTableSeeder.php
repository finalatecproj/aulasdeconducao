<?php

use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //state = 0 desativado
        //state = 1 ativado

        //joao
        DB::table('students')->insert([
            'name' => 'Rui z',
            'phone' => '915423456',
            'email' => 'rui@gmail.com',
            'state' => 1
        ]);

        DB::table('students')->insert([
            'name' => 'Rute x',
            'phone' => '998741236',
            'email' => 'rute@gmail.com',
            'state' => 1
        ]);
        DB::table('students')->insert([
            'name' => 'Ruben y',
            'phone' => '998741236',
            'email' => 'ruben@gmail.com',
            'state' => 1
        ]);
        DB::table('students')->insert([
            'name' => 'rita p',
            'phone' => '998741236',
            'email' => 'rita@gmail.com',
            'state' => 1
        ]);
        DB::table('students')->insert([
            'name' => 'Ricardo p',
            'phone' => '998741236',
            'email' => 'ricardo@gmail.com',
            'state' => 1
        ]);
    }
}
