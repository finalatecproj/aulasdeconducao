<?php

use App\Instructor;
use App\Vehicle;
use App\Student;
use App\CategoryStudent;
use Illuminate\Database\Seeder;

class LessonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lessons')->insert([
            'instructor_id' => Instructor::where('name', 'like', 'xavier c')->get()->first()->id,
            'vehicle_id' => Vehicle::where('model', 'like', 'supra')->get()->first()->id,
            'student_id' => Student::where('name', 'like', 'Rui z')->first()->id,
            'start_time' => '2017-11-02 09:00:00',
            'finish_time' => '2017-11-02 10:00:00'
        ]);
        DB::table('lessons')->insert([
            'instructor_id' => Instructor::where('name', 'like', 'marcelo a')->get()->first()->id,
            'vehicle_id' => Vehicle::where('model', 'like', 'jt')->get()->first()->id,
            'student_id' => Student::where('name', 'like', 'Rui z')->first()->id,
            'start_time' => '2017-11-07 16:00:00',
            'finish_time' => '2017-11-07 17:00:00'
        ]);
        DB::table('lessons')->insert([
            'instructor_id' => Instructor::where('name', 'like', 'ana e')->get()->first()->id,
            'vehicle_id' => Vehicle::where('model', 'like', 'supra')->get()->first()->id,
            'student_id' => Student::where('name', 'like', 'Rui z')->first()->id,
            'start_time' => '2017-11-16 16:00:00',
            'finish_time' => '2017-11-16 17:00:00'
        ]);
        
        DB::table('lessons')->insert([
            'instructor_id' => Instructor::where('name', 'like', 'joao b')->get()->first()->id,
            'vehicle_id' => Vehicle::where('model', 'like', 'seila2')->get()->first()->id,
            'student_id' => Student::where('name', 'like', 'Ruben y')->first()->id,
            'start_time' => '2017-11-02 09:00:00',
            'finish_time' => '2017-11-02 10:00:00'
        ]);

        DB::table('lessons')->insert([
            'instructor_id' => Instructor::where('name', 'like', 'ana e')->get()->first()->id,
            'vehicle_id' => Vehicle::where('model', 'like', 'seila')->get()->first()->id,
            'student_id' => Student::where('name', 'like', 'Ruben y')->first()->id,
            'start_time' => '2017-11-07 16:00:00',
            'finish_time' => '2017-11-07 17:00:00'
        ]);
    }
}
