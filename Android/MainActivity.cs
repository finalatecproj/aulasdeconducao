﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Webkit;
using Android.Views;


namespace FinalProjectPlanB
{
    [Activity(Label = "FinalProjectPlanB", MainLauncher = true)]
    public class MainActivity : Activity
    {
        WebView localWebView;
        protected override void OnCreate(Bundle bundle)
        {
            
            base.OnCreate(bundle);

            RequestWindowFeature(WindowFeatures.NoTitle);

            SetContentView(Resource.Layout.Main);

            localWebView = FindViewById<WebView>(Resource.Id.LocalWebView);

            localWebView.SetWebViewClient(new WebViewClient()); // stops request going to Web Browser

            localWebView.LoadUrl("http://127.0.0.1:8000");

            localWebView.Settings.JavaScriptEnabled = true;
        }

        public override void OnBackPressed()
        {
            localWebView.GoBack();
        }
    }
}

